package com.example.basicmaths.Adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.basicmaths.R;
import com.example.basicmaths.model.AdditionModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AdditionQuestionListAdapter extends RecyclerView.Adapter<AdditionQuestionListAdapter.QuestionHolder> {
    Context context;
    ArrayList<AdditionModel> additionList;
    int questionCounter;
    int totalQuestionCount;
    AdditionModel currentQuestion;
    int score;

    public AdditionQuestionListAdapter(Context context,ArrayList<AdditionModel> additionList) {
        this.context=context;
        this.additionList = additionList;
    }

    @NonNull
    @Override
    public QuestionHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new QuestionHolder(LayoutInflater.from(context).inflate(R.layout.view_row_option, null));
    }

    @Override
    public void onBindViewHolder(@NonNull QuestionHolder holder, int position) {
            totalQuestionCount= additionList.size();
            if(questionCounter<totalQuestionCount){
                currentQuestion= additionList.get(questionCounter);

            }
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    @OnClick(R.id.tvQuestion1)
    public void onTvQuestion1Clicked() {
    }

    @OnClick(R.id.tvQuestion2)
    public void onTvQuestion2Clicked() {
    }

    @OnClick(R.id.tvQuestion3)
    public void onTvQuestion3Clicked() {
    }

    @OnClick(R.id.tvQuestion4)
    public void onTvQuestion4Clicked() {
    }

    class QuestionHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvQuestionCount)
        TextView tvQuestionCount;
        @BindView(R.id.tvScore)
        TextView tvScore;
        @BindView(R.id.tvQuestion)
        TextView tvQuestion;
        @BindView(R.id.tvQuestion1)
        TextView tvQuestion1;
        @BindView(R.id.tvQuestion2)
        TextView tvQuestion2;
        @BindView(R.id.tvQuestion3)
        TextView tvQuestion3;
        @BindView(R.id.tvQuestion4)
        TextView tvQuestion4;

        public QuestionHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
