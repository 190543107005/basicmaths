package com.example.basicmaths;

import android.content.Intent;
import android.os.Bundle;

import androidx.cardview.widget.CardView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity {

    @BindView(R.id.cvAddition)
    CardView cvAddition;
    @BindView(R.id.cvSubstraction)
    CardView cvSubstraction;
    @BindView(R.id.cvMultiplication)
    CardView cvMultiplication;
    @BindView(R.id.cvDivision)
    CardView cvDivision;
    @BindView(R.id.btnAdmin)
    FloatingActionButton btnAdmin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setupActionBar("Dashbord", false);
    }

    @OnClick(R.id.cvAddition)
    public void onCvAdditionClicked() {
        Intent i = new Intent(MainActivity.this, AdditionDigitActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.cvSubstraction)
    public void onCvSubstractionClicked() {
        Intent i = new Intent(MainActivity.this, SubstractionDigitActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.cvMultiplication)
    public void onCvMultiplicationClicked() {
        Intent i = new Intent(MainActivity.this, MultiplicationDigitActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.cvDivision)
    public void onCvDivisionClicked() {
        Intent i = new Intent(MainActivity.this, DivisionDigitActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.btnAdmin)
    public void onViewClicked() {
        Intent intent = new Intent(MainActivity.this,Actyvity_AdmiNLogin.class);
        startActivity(intent);
    }
}