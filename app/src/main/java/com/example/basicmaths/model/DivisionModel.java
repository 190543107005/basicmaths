package com.example.basicmaths.model;

import java.io.Serializable;

public class DivisionModel implements Serializable {
    int D_id;
    int Value1;
    int Value2;
    int Option1;
    int Option2;
    int Option3;
    int Option4;
    int Result;

    @Override
    public String toString() {
        return "DivisionModel{" +
                "D_id=" + D_id +
                ", Value1=" + Value1 +
                ", Value2=" + Value2 +
                ", Option1=" + Option1 +
                ", Option2=" + Option2 +
                ", Option3=" + Option3 +
                ", Option4=" + Option4 +
                ", Result=" + Result +
                '}';
    }
}
