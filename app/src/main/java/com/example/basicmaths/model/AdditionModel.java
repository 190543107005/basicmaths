package com.example.basicmaths.model;

import java.io.Serializable;

public class AdditionModel implements Serializable {
    int A_id;
    int Value1;
    int Value2;
    int Option1;
    int Option2;
    int Option3;
    int Option4;
    int Result;

    public int getA_id() {
        return A_id;
    }

    public void setA_id(int a_id) {
        A_id = a_id;
    }

    public int getValue1() {
        return Value1;
    }

    public void setValue1(int value1) {
        Value1 = value1;
    }

    public int getValue2() {
        return Value2;
    }

    public void setValue2(int value2) {
        Value2 = value2;
    }

    public int getOption1() {
        return Option1;
    }

    public void setOption1(int option1) {
        Option1 = option1;
    }

    public int getOption2() {
        return Option2;
    }

    public void setOption2(int option2) {
        Option2 = option2;
    }

    public int getOption3() {
        return Option3;
    }

    public void setOption3(int option3) {
        Option3 = option3;
    }

    public int getOption4() {
        return Option4;
    }

    public void setOption4(int option4) {
        Option4 = option4;
    }

    public int getResult() {
        return Result;
    }

    public void setResult(int result) {
        Result = result;
    }

    @Override
    public String toString() {
        return "AdditionModel{" +
                "A_id=" + A_id +
                ", Value1=" + Value1 +
                ", Value2=" + Value2 +
                ", Option1=" + Option1 +
                ", Option2=" + Option2 +
                ", Option3=" + Option3 +
                ", Option4=" + Option4 +
                ", Result=" + Result +
                '}';
    }
}
