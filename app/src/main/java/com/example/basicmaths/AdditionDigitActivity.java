package com.example.basicmaths;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AdditionDigitActivity extends BaseActivity {
    @BindView(R.id.cv1digitwith1)
    CardView cv1digitwith1;
    @BindView(R.id.cv1digitwith2)
    CardView cv1digitwith2;
    @BindView(R.id.cv2digitwith2)
    CardView cv2digitwith2;
    @BindView(R.id.cv2digitwith3)
    CardView cv2digitwith3;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.digits_additon);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.cv1digitwith1)
    public void onCv1digitwith1Clicked() {
    }

    @OnClick(R.id.cv1digitwith2)
    public void onCv1digitwith2Clicked() {
    }

    @OnClick(R.id.cv2digitwith2)
    public void onCv2digitwith2Clicked() {
    }

    @OnClick(R.id.cv2digitwith3)
    public void onCv2digitwith3Clicked() {
    }
}
