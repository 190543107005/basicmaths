package com.example.basicmaths;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.example.basicmaths.database.Tbl_Addition;
import com.example.basicmaths.model.AdditionModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InsertAdditionData extends BaseActivity {
    @BindView(R.id.etVal1)
    EditText etVal1;
    @BindView(R.id.etVal2)
    EditText etVal2;
    @BindView(R.id.tvResult)
    TextView tvResult;
    @BindView(R.id.etValue1)
    EditText etValue1;
    @BindView(R.id.etValue2)
    EditText etValue2;
    @BindView(R.id.etoption1)
    EditText etoption1;
    @BindView(R.id.etoption2)
    EditText etoption2;
    @BindView(R.id.etoption3)
    EditText etoption3;
    @BindView(R.id.etoption4)
    EditText etoption4;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;

    AdditionModel additionModel;
    @BindView(R.id.tvAdd)
    Button tvAdd;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.insertdata);
        ButterKnife.bind(this);
        setupActionBar("Insert Data", true);
    }

    @OnClick(R.id.btnSubmit)
    public void onViewClicked() {
        if (additionModel == null) {
            long lastInsertedId = new Tbl_Addition(getApplicationContext()).insertData(etValue1.getText().toString(),
                    etValue2.getText().toString(),
                    Integer.parseInt(etoption1.getText().toString()),
                    Integer.parseInt(etoption2.getText().toString()),
                    Integer.parseInt(etoption3.getText().toString()),
                    Integer.parseInt(etoption4.getText().toString()));
            if (lastInsertedId > 0)
                Toast.makeText(getApplicationContext(), "Inserted Successfully", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
        }
    }

    void parseData() {
        tvResult.setText(Integer.parseInt(etVal1.getText().toString()) + Integer.parseInt(etVal2.getText().toString()));
    }


}
