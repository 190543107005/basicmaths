package com.example.basicmaths;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SelectOperationToInsert extends BaseActivity {
    @BindView(R.id.cvAddition)
    CardView cvAddition;
    @BindView(R.id.cvSubstraction)
    CardView cvSubstraction;
    @BindView(R.id.cvMultiplication)
    CardView cvMultiplication;
    @BindView(R.id.cvDivision)
    CardView cvDivision;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.choose_operation_toinsert);
        ButterKnife.bind(this);
        setupActionBar("SelectOperation",true);
    }

    @OnClick(R.id.cvAddition)
    public void onCvAdditionClicked() {
        Intent intent = new Intent(this,InsertAdditionData.class);
        startActivity(intent);
    }

    @OnClick(R.id.cvSubstraction)
    public void onCvSubstractionClicked() {
        Intent intent = new Intent(this,InsertSubstractionData.class);
        startActivity(intent);
    }

    @OnClick(R.id.cvMultiplication)
    public void onCvMultiplicationClicked() {
        Intent intent = new Intent(this,InsertMultyplyData.class);
        startActivity(intent);
    }

    @OnClick(R.id.cvDivision)
    public void onCvDivisionClicked() {
        Intent intent = new Intent(this,InsertDivisionData.class);
        startActivity(intent);
    }
}
