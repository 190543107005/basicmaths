package com.example.basicmaths;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Actyvity_AdmiNLogin extends BaseActivity {
    @BindView(R.id.etAdmin)
    EditText etAdmin;
    @BindView(R.id.etPassword)
    EditText etPassword;
    @BindView(R.id.btnLogin)
    Button btnLogin;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.admin_login);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btnLogin)
    public void onViewClicked() {
        validAdmin();
    }
    void validAdmin(){
        if (etAdmin.getText().toString().equals("Admin" ) && etPassword.getText().toString().equals("A_264")){
            Toast.makeText(getApplicationContext(), "Login Successfully", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(Actyvity_AdmiNLogin.this,SelectOperationToInsert.class);
            startActivity(intent);
        }else
            Toast.makeText(getApplicationContext(), "Incorrect UserName or Password", Toast.LENGTH_SHORT).show();
    }
}
