package com.example.basicmaths.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class Tbl_Division extends MyDatabase {
    public static final String TABLE_NAME = "Tbl_Division";
    public static final String ADDITION_ID= "D_id";
    public static final String VALUE1= "Value1";
    public static final String VALUE2= "Value2";
    public static final String OPTION1= "Option1";
    public static final String OPTION2= "Option2";
    public static final String OPTION3= "Option3";
    public static final String OPTION4= "Option4";
    public static final String RESULT= "Result";
    public Tbl_Division(Context context) {
        super(context);
    }

    public long insertData(String value1,String value2,int option1,int option2,int option3,int option4){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(VALUE1,value1);
        cv.put(VALUE2,value2);
        cv.put(OPTION1,option1);
        cv.put(OPTION2,option2);
        cv.put(OPTION3,option3);
        cv.put(OPTION4,option4);
        long lastInsertedId = db.insert(TABLE_NAME,null,cv);
        db.close();
        return lastInsertedId;
    }
}
