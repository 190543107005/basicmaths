package com.example.basicmaths.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

public class MyDatabase extends SQLiteAssetHelper {
    public static final String DATABASR_NAME = "BasicMaths.db";
    public MyDatabase(Context context) {
        super(context,DATABASR_NAME, null, 1);
    }
}
