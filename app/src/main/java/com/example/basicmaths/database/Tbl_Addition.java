package com.example.basicmaths.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.basicmaths.model.AdditionModel;

import java.util.ArrayList;

public class Tbl_Addition extends MyDatabase {
    public static final String TABLE_NAME = "Tbl_Addition";
    public static final String ADDITION_ID= "A_id";
    public static final String VALUE1= "Value1";
    public static final String VALUE2= "Value2";
    public static final String OPTION1= "Option1";
    public static final String OPTION2= "Option2";
    public static final String OPTION3= "Option3";
    public static final String OPTION4= "Option4";
    public static final String RESULT= "Result";
    public Tbl_Addition(Context context) {
        super(context);
    }

    public AdditionModel getCreateModuleUsingCursor(Cursor cursor){
        AdditionModel additionModel = new AdditionModel();
        additionModel.setA_id(cursor.getInt(cursor.getColumnIndex(ADDITION_ID)));
        additionModel.setValue1(Integer.parseInt(cursor.getString(cursor.getColumnIndex(VALUE1))));
        additionModel.setValue2(Integer.parseInt(cursor.getString(cursor.getColumnIndex(VALUE2))));
        additionModel.setOption1(cursor.getInt(cursor.getColumnIndex(OPTION1)));
        additionModel.setOption2(cursor.getInt(cursor.getColumnIndex(OPTION2)));
        additionModel.setOption3(cursor.getInt(cursor.getColumnIndex(OPTION3)));
        additionModel.setOption4(cursor.getInt(cursor.getColumnIndex(OPTION4)));
        additionModel.setResult(Integer.parseInt(cursor.getString(cursor.getColumnIndex(VALUE1)))+Integer.parseInt(cursor.getString(cursor.getColumnIndex(VALUE2))));
    return additionModel;
    }
    public ArrayList<AdditionModel> getQuestionList(){
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<AdditionModel> list = new ArrayList<>();
        String query = "SELECT * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(TABLE_NAME,null);
        cursor.moveToFirst();
        for(int i = 0; i<cursor.getCount(); i++){
            list.add(getCreateModuleUsingCursor(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }

    public long insertData(String value1,String value2,int option1,int option2,int option3,int option4){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(VALUE1,value1);
        cv.put(VALUE2,value2);
        cv.put(OPTION1,option1);
        cv.put(OPTION2,option2);
        cv.put(OPTION3,option3);
        cv.put(OPTION4,option4);
        long lastInsertedId = db.insert(TABLE_NAME,null,cv);
        db.close();
        return lastInsertedId;
    }

}
